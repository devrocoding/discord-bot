package net.enchantedmc.discord.invite.command;

import com.google.gson.JsonParser;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.ChannelType;
import net.dv8tion.jda.core.entities.Message;
import net.dv8tion.jda.core.entities.MessageChannel;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.enchantedmc.discord.DiscordBot;
import net.enchantedmc.discord.invite.InviteListener;
import net.enchantedmc.discord.util.PrivateMessage;
import net.enchantedmc.discord.util.UtilMaps;

import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Random;

public class CheckInvitesCommand extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) return;
        Message message = event.getMessage();
        String content = message.getContentRaw();
        // getContentRaw() is an atomic getter
        // getContentDisplay() is a lazy getter which modifies the content for e.g. console view (strip discord formatting)
        MessageChannel channel = event.getChannel();
        if (event.getChannelType() != ChannelType.PRIVATE) {
            if (content.equals("!!checkinvites")) {
                int invites = UtilMaps.getData().getInvitesFromUser(event.getMember().getUser(), event.getGuild());

                if (UtilMaps.getData().invites.isEmpty() || invites <= 0) {
                    channel.sendMessage(event.getMember().getAsMention() + ", you don't have an invite link!").queue();
                    return;
                }
                channel.sendMessage("Hey " + event.getMember().getAsMention() + ", u have " +
                        invites + " invites!").queue();
            } else if (content.equals("!!reloadranks")) {
                if (event.getMember().hasPermission(Permission.MANAGE_SERVER)) {
                    try {
                        JsonParser parser = new JsonParser();
                        DiscordBot.jsonObject = parser.parse(new InputStreamReader(new FileInputStream("credentials.json"))).getAsJsonObject();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    InviteListener.instance.checkInvites();
                    channel.sendMessage("I reloaded and checked everyone's rank! BLIEP BLEP").queue();
                } else {
                    channel.sendMessage("You can not bypass my security, little hacker!").queue();
                }
            }
            if (content.contains("!!say")) {
                if (event.getMember().hasPermission(Permission.MANAGE_SERVER)) {
                    String bericht = event.getMessage().getContentRaw().replace("!!say", "");
                    channel.sendMessage(bericht).queue();
                } else {
                    channel.sendMessage("You can not bypass my security, little hacker!").queue();
                }
            }
        }else{
            String prefix = "";
            for (PrivateMessage pm : PrivateMessage.values()){
                for (String command : pm.getCommand()){
                    if (content.equalsIgnoreCase(prefix+command) || content.contains(prefix+command)){
                        String rResponse = pm.getResponse()[new Random().nextInt(pm.getResponse().length)];
                        String response = rResponse.replaceAll("#PLAYER#",
                                event.getAuthor().getName());
                        channel.sendMessage(response).queue();
                        break;
                    }
                }
            }
        }
    }

}
