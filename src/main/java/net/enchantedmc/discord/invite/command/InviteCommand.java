package net.enchantedmc.discord.invite.command;

import com.jagrosh.jdautilities.commandclient.Command;
import com.jagrosh.jdautilities.commandclient.CommandEvent;
import java.util.function.Consumer;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Role;

public class InviteCommand extends Command {

    private final Consumer<Integer> consumer;

    public InviteCommand(Consumer<Integer> consumer) {
        this.consumer = consumer;

        this.name = "checkinvites";
        this.guildOnly = true;
    }

    @Override
    protected void execute(CommandEvent event) {
        for (Role role : event.getMember().getRoles()) {
            if (role.hasPermission(Permission.ADMINISTRATOR)) {
                consumer.accept(0);
                event.getChannel().sendMessage("Checking invites...");
                return;
            }
        }
    }
}
