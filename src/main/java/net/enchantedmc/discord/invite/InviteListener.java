package net.enchantedmc.discord.invite;

import com.google.common.collect.Lists;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.entities.Guild;
import net.dv8tion.jda.core.entities.Invite;
import net.dv8tion.jda.core.entities.Member;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.entities.User;
import net.dv8tion.jda.core.events.ReadyEvent;
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;
import net.enchantedmc.discord.DiscordBot;
import net.enchantedmc.discord.util.Pair;
import net.enchantedmc.discord.util.UtilMaps;

public class InviteListener extends ListenerAdapter {

    public static InviteListener instance;

    private final Consumer<Integer> consumer;

    public InviteListener() {
        instance = this;
        this.consumer = new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                new Thread(() -> {
                    checkInvites();
                }).run();
            }
        };
    }

    public void checkInvites(){
        for (Guild guild : DiscordBot.getGuildIds()) {
            for (Member member : guild.getMembers()) {
                User user = member.getUser();
                AtomicInteger userInvites = new AtomicInteger(0);

                guild.getInvites().queue(invites -> {
                    invites.forEach(invite -> {
                        if (invite.getInviter().getId().equals(user.getId())) {
                            userInvites.set(userInvites.get() + invite.getUses());
                            Map<Guild, Integer> pair = new HashMap<>();
                            pair.put(guild, userInvites.get());
                            if (UtilMaps.getData().invites.containsKey(user)){
                                UtilMaps.getData().invites.get(user).add(pair);
                            }else{
                                List<Map<Guild, Integer>> list = new ArrayList<>();
                                list.add(pair);
                                UtilMaps.getData().invites.put(user, list);
                            }
                        }
                    });

//                    if (userInvites.get() > 0) {
//                        System.out.println(userInvites.get());
//                    }

                    List<Pair<String, Integer>> list = UtilMaps.getData().getDataFromServer(guild.getId());
                    if (list != null) {
                        for (Pair<String, Integer> pair : list) {
                            String rank = pair.getLeft();
                            int votes = pair.getRight();
                            if (userInvites.get() >= votes) {
                                handleRank(member, votes, rank, guild);
                                break;
                            }
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onGuildMemberJoin(GuildMemberJoinEvent event) {
        consumer.accept(0);
        System.out.println(event.getUser().getName() + " Joined checking invites...");
    }

    @Override
    public void onReady(ReadyEvent event) {
        System.out.println("JDA successfully loaded, checking invites...");

        List<Pair<String, Integer>> map = checkLeaderboard();
        map.sort((o1, o2) -> o1.getRight() - o2.getRight());
        Collections.reverse(map);
    }

    private boolean handleRank(Member member, int amount, String role, Guild guild) {
        JDA jda = DiscordBot.getJDA();
        Role roleObject = guild.getRolesByName(role, true).get(0);

        if (member.getRoles().contains(guild.getRolesByName(role, true).get(0))) {
            return false;
        }

        guild.getController().addRolesToMember(member, roleObject).queue(aVoid -> {
            guild.getTextChannelsByName("reward_log", true).get(0).sendMessage(member.getAsMention() + " Has gotten " + amount + " invites and ranked up to " + roleObject.getName()).queue();
        });
        return true;
    }

    public Consumer<Integer> getConsumer() {
        return consumer;
    }

    private List<Pair<String, Integer>> checkLeaderboard() {
        List<Pair<String, Integer>> leaderboard = Lists.newArrayList();

        for (Guild guild : DiscordBot.getGuildIds()) {
            for (Member member : guild.getMembers()) {
                User user = member.getUser();
                AtomicInteger userInvites = new AtomicInteger(0);

                List<Invite> invites = guild.getInvites().complete();

                invites.forEach(invite -> {
                    if (invite.getInviter().getId().equals(user.getId())) {
                        userInvites.set(userInvites.get() + invite.getUses());
                    }
                });

                if (userInvites.get() > 0) {
                    leaderboard.add(new Pair<String, Integer>(user.getId(), userInvites.get()));
                }
            }
        }

        return leaderboard;
    }
}