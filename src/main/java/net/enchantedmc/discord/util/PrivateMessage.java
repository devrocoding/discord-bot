package net.enchantedmc.discord.util;

public enum PrivateMessage {

    HEY(new String[]{"Hey"}, "How u doing", "Whatsuppp..", "Welcome to our private conversation #PLAYER# :D"),
    HOW_ARE_YOU(new String[]{"are u"}, "I'm fine, thanks #PLAYER#", "It can be better, but thanks..", "Why is the world not flat?"),
    JOKE(new String[]{"joke"}, "The bartender says, 'we don't serve robots.' The robot replies, 'oh, but some day you will.'", "Joke? What's that"),
    NAME(new String[]{"name"}, "My name is Invi, and your name is #PLAYER# i think....", "Name? English please"),
    YES(new String[]{"yes"}, "Great...", "I'm so happy for you", "Glad that i helped");

    private String[] command;
    private String[] response;

    PrivateMessage(String[] command, String... response){
        this.command = command;
        this.response = response;
    }

    public String[] getCommand(){
        return this.command;
    }

    public String[] getResponse(){
        return this.response;
    }

}
