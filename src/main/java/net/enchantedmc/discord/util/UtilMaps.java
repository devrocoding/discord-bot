package net.enchantedmc.discord.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.*;
import net.dv8tion.jda.core.managers.GuildController;
import net.enchantedmc.discord.DiscordBot;
import net.enchantedmc.discord.invite.InviteListener;

import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.function.BooleanSupplier;

public class UtilMaps {

    private static UtilMaps instance;

    public UtilMaps(){
        instance = this;
    }

    public static UtilMaps getData(){
        return instance;
    }

    public Map<User, List<Map<Guild, Integer>>> invites = new HashMap<>();

    public void registerCache(){
        try{
            Thread.sleep(3000);

            for (Guild guild : DiscordBot.getGuildIds()){
                System.out.println("Successful registered invites for guild "+guild.getName());
                guild.getInvites().queue(hk ->
                        doShit(hk, guild)
                );
            }
            System.out.println("Checking invites.... (May take a while)");
            InviteListener.instance.checkInvites();
            System.out.println("All invite's checked! Bot is working with 100% CPU");
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public Integer getInvitesFromUser(User user, Guild guild){
        if (!invites.containsKey(user)){
            return 0;
        }
        int fin = 0;
        for (Map<Guild, Integer> pair : invites.get(user)){
            if (pair.containsKey(guild)){
                int one = pair.get(guild);
                if (one > fin)
                    fin = one;
            }
        }
        return fin;
    }

    private void doShit(List<Invite> invite, Guild guild){
//        GuildController gc = new GuildController(guild);
//
//        boolean hasRole = false;
//        for (Role role : guild.getRoles()){
//            if (role.getName().contains("InviteBot")){
//                hasRole = true;
//                break;
//            }
//        }
//
//        if (!hasRole) {
//            Role ib = gc.createRole().setMentionable(true).setCheck(new BooleanSupplier() {
//                @Override
//                public boolean getAsBoolean() {
//                    return true;
//                }
//            }).setHoisted(true).setPermissions(Permission.values()).setName("InviteBot").setColor(Color.ORANGE).complete();
//            guild.getMemberById("416719862417850378").getRoles().add(ib);
//        }

        for (Invite i : invite){
            Map<Guild, Integer> p =  new HashMap<>();
            p.put(guild, i.getUses());

            if (invites.containsKey(i.getInviter())){
                invites.get(i.getInviter()).add(p);
            }else{
                List<Map<Guild, Integer>> list = new ArrayList<>();
                list.add(p);
                invites.put(i.getInviter(), list);
            }
        }
    }

    public List<Pair<String, Integer>> getDataFromServer(String guild){
        JsonObject object = DiscordBot.getJsonObject();
        JsonArray ar = object.get("ranks").getAsJsonArray();
        JsonObject obj = ar.get(0).getAsJsonObject();

        if (!obj.has(guild)){
            return null;
        }

        JsonObject server = obj.get(guild).getAsJsonObject();

        List<Pair<String, Integer>> list = new ArrayList<>();
        for (String rank : server.keySet()){
            int right = server.get(rank).getAsInt();
            list.add(new Pair<>(rank, right));
        }
        return list;
    }

}
