package net.enchantedmc.discord.chat;

import net.dv8tion.jda.core.Permission;
import net.dv8tion.jda.core.entities.Role;
import net.dv8tion.jda.core.events.message.MessageReceivedEvent;
import net.dv8tion.jda.core.hooks.ListenerAdapter;

public class ChatListener extends ListenerAdapter {

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getMessage().getContentRaw().contains("@")) {
            for (Role role : event.getMember().getRoles()) {
                if (role.hasPermission(Permission.BAN_MEMBERS)) {
                    return;
                }
            }
            event.getMessage().delete().queue();
        }
    }


}
