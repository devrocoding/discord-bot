package net.enchantedmc.discord;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jagrosh.jdautilities.waiter.EventWaiter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import net.dv8tion.jda.core.AccountType;
import net.dv8tion.jda.core.JDA;
import net.dv8tion.jda.core.JDABuilder;
import net.dv8tion.jda.core.entities.Guild;
import net.enchantedmc.discord.chat.ChatListener;
import net.enchantedmc.discord.invite.InviteListener;
import net.enchantedmc.discord.invite.command.CheckInvitesCommand;
import net.enchantedmc.discord.mongo.DatabaseManager;
import net.enchantedmc.discord.util.UtilMaps;

public class DiscordBot {

    private static String TOKEN;
    private static List<Guild> GUILD_ID;

    private static final EventWaiter WAITER = new EventWaiter();

    private static JDA BOT;
    private static DatabaseManager DATABASE_MANAGER;
    public static JsonObject jsonObject;

    public static void main(String[] args) {
        try {
            new UtilMaps();

            File file = new File("credentials.json");

            if (!file.exists()) {
                file.createNewFile();
                JsonObject jsonObject = new JsonObject();
                jsonObject.addProperty("token", "NDE2NzE5ODYyNDE3ODUwMzc4.DXIkPA.ma-8G3nTGKkf4Cbj8EXXYh_MS98");
                jsonObject.addProperty("guild", "245261936634101760");

                jsonObject.addProperty("hostname", "144.217.84.175");
                jsonObject.addProperty("port", 27017);
                jsonObject.addProperty("database", "admin");
                jsonObject.addProperty("username", "discord");
                jsonObject.addProperty("password", "GrotePlasser88");

                JsonArray object = new JsonArray();
                JsonObject obj = new JsonObject(),
                        obj2 = new JsonObject(),
                    server = new JsonObject();

                server.add("245261936634101760", obj);

                obj.addProperty("Thief", 20);
                obj.addProperty("Murderer", 11);
                obj.addProperty("Prisoner", 10);
                obj.addProperty("Convicted", 1);

                server.add("409554901639626753", obj2);
                obj2.addProperty("Rank 4", 100);
                obj2.addProperty("Rank 3", 50);
                obj2.addProperty("Rank 2", 20);
                obj2.addProperty("Rank 1", 1);
                object.add(server);

                jsonObject.add("ranks", object);

                try (FileWriter fileWriter = new FileWriter(file.getAbsolutePath())) {
                    fileWriter.write(jsonObject.toString());
                    fileWriter.flush();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

            JsonParser parser = new JsonParser();
            jsonObject = parser.parse(new InputStreamReader(new FileInputStream("credentials.json"))).getAsJsonObject();

            TOKEN = jsonObject.get("token").getAsString();
            DATABASE_MANAGER = new DatabaseManager(jsonObject.get("hostname").getAsString(), jsonObject.get("port").getAsInt(), jsonObject.get("database").getAsString(), jsonObject.get("username").getAsString(), jsonObject.get("password").getAsString());

            BOT = new JDABuilder(AccountType.BOT).setToken(TOKEN).buildAsync();

            InviteListener inviteListener = new InviteListener();

            BOT.setAutoReconnect(true);
            BOT.addEventListener(WAITER);
            BOT.addEventListener(inviteListener, new ChatListener());
            BOT.addEventListener(new CheckInvitesCommand());

            UtilMaps.getData().registerCache();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static JDA getJDA() {
        return BOT;
    }

    public static JsonObject getJsonObject(){
        return jsonObject;
    }

    public static List<Guild> getGuildIds() {
        return getJDA().getGuilds();
    }

    public static DatabaseManager getDatabaseManager() {
        return DATABASE_MANAGER;
    }
}
