package net.enchantedmc.discord.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;
import java.util.Arrays;

public class DatabaseManager {

    private static DatabaseManager instance;
    private final MongoClient mongo;
    private final MongoDatabase database;

    public DatabaseManager(String hostname, int portnumber, String databaseName, String username, String password) {

        System.out.println("Connecting to mongo");
        mongo = new MongoClient(new ServerAddress(hostname, portnumber), Arrays.asList(MongoCredential.createCredential(username, databaseName, password.toCharArray())));
        database = getMongo().getDatabase(databaseName);
    }


    public MongoClient getMongo() {
        return mongo;
    }

    public MongoDatabase getDatabase() {
        return database;
    }
}
